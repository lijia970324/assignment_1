# assignment_1



## Description of the repository

File "main.ipynb" is the main python file of this project.

File "zomato_df_final_data.csv" is the raw data file.

File "sydney.geojson" is the Sydney map geojson file we used to plot density maps in the project.

## How to run it

Simply just run the "main.ipynb" file in Jupyter Notebook, and you will get all results.

## Expected results

Some description texts explaining why I code like this, and also analysis for the results got.

Some histograms, bar charts, and maps etc. for the data visualisation.

Some confusion matrix and statistic numbers for the predictive model built. 


